package com.company;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Cat cat1 = new Cat();
        cat1.setName("tiger");
        cat1.setBirthday(new Date());
        Cat cat2 = new Cat();
        cat2.setName("tiger");
        cat2.setBirthday(new Date());
        System.out.println(cat1.equals(cat2));
        Cat cat3 = new Cat("tiger", new Date());
        System.out.println(cat3.equals(cat1));

        Age(new Dog(5));
        Age(new Bird(2));
    }

    public static void Age(Animal age) {
        age.Age();
    }
}
