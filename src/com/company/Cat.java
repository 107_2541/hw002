package com.company;

import java.util.Date;

public class Cat {
    private String name;
    private Date birthday;

    public Cat() {
    }

    public Cat(String name,Date birthday){
        this.name = name;
        this.birthday = birthday;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getBirthday() {
        return birthday;
    }

    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Cat))
            return false;
        final Cat cat = (Cat) other;
        if (!getName().equals(cat.getName()))
            return false;
        if (!getBirthday().equals(cat.getBirthday()))
            return false;
        return false;
    }

    public int hashCode() {
        int result = getName().hashCode();
        result = 29 * result + getBirthday().hashCode();
        return result;
    }
    /*@Override
    public String toString(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getName()).append("\n").append(getBirthday());
        return stringBuffer.toString();
    }*/
}
